package info.hccis.flowershop.controllers;

import com.google.gson.Gson;
import info.hccis.flowershop.bo.CustomerTypeBO;
import info.hccis.flowershop.bo.FlowerOrderBO;
import info.hccis.flowershop.dao.CustomerDAO;
import info.hccis.flowershop.entity.jpa.Customer;
import info.hccis.flowershop.entity.jpa.FlowerOrder;
import info.hccis.flowershop.repositories.CustomerRepository;
import info.hccis.flowershop.repositories.FlowerOrderRepository;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for the customer functionality of the site
 *
 * @since 20201022
 * @author PAAG
 */
@Controller
@RequestMapping("/customers")
public class CustomerController
{
    
    private final CustomerRepository customerRepository;
    private final FlowerOrderRepository flowerOrderRepository;

    public CustomerController(CustomerRepository br, FlowerOrderRepository fr) {
        customerRepository = br;
        flowerOrderRepository = fr;
    }
    
    /**
     * Page to allow user to view customers
     *
     * @since 20201022
     * @author PAAG (modified from BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/list")
    public String list(Model model) {

        //Go get the bookings from the database.
        ArrayList<Customer> customers = loadCustomers();
        model.addAttribute("customers", customers);

        return "customers/list";
    }
    
    /**
     * Page to allow user to add customers
     *
     * @since 20201022
     * @author PAAG (modified from BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/add")
    public String add(Model model) {

        Customer customer = new Customer();
        customer.setId(0);
        model.addAttribute("customer", customer);

        return "customers/add";
    }
    
    /**
     * Page to allow user to find customers
     *
     * @since 20201022
     * @author PAAG (modified from BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/find")
    public String find(Model model) {

        //Load the names into the model
        Set<String> names = new HashSet();
        
        ArrayList<Customer> customers = loadCustomers();
        for(Customer current: customers){
            names.add(current.getFullName());
        }
                
        model.addAttribute("names", names);
        
        return "customers/find";
    }
    
    /**
     * Page to allow user to edit a customer
     *
     * @since 20201022
     * @author PAAG (modified from BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/edit")
    public String edit(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

        Optional<Customer> selectedCustomer = customerRepository.findById(id);

        if (selectedCustomer == null) {
            return "index";
        } else {
            model.addAttribute("customer", selectedCustomer);
            return "customers/add";
        }
    }
    
    /**
     * Page to allow user to delete customers
     *
     * @since 20201022
     * @author PAAG (modified from BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/delete")
    public String delete(Model model, HttpServletRequest request) {

        String idString = (String) request.getParameter("id");
        int id = Integer.parseInt(idString);
        System.out.println("BJTEST - id=" + id);

        customerRepository.deleteById(id);

        ArrayList<Customer> customers = loadCustomers();
        model.addAttribute("customers", customers);

        //send the user to the list page.
        return "customers/list";

    }
    
    /**
     * Page to allow user to add customer
     *
     * @since 20201022
     * @author PAAG (modified from BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model, @Valid @ModelAttribute("customer") Customer customer, BindingResult result) {

        //If errors put the object back in model and send back to the add page.
        if (result.hasErrors()) {
            System.out.println("Validation error");
            return "customers/add";
        }

        customerRepository.save(customer);

        //reload the list of customers
        ArrayList<Customer> customers = loadCustomers();
        model.addAttribute("customers", customers);

        //send the user to the list page.
        return "customers/list";
    }
    
    /**
     * Page to allow user to view a customer
     *
     * @since 20201022
     * @author PAAG (modified from BJM (modified from Fred/Amro's project
     */
    @RequestMapping("/findSubmit")
    public String findSubmit(Model model, @ModelAttribute("customer") Customer customer) {

        //Retrieve all orders from db
        FlowerOrderBO orderBO = new FlowerOrderBO();
        ArrayList<FlowerOrder> allOrders = (ArrayList<FlowerOrder>) flowerOrderRepository.findAll();//ByCustomerId(customer.getId());
        ArrayList<FlowerOrder> orders = new ArrayList<>();
        ArrayList<Customer> allCustomers = (ArrayList<Customer>) customerRepository.findAllByFullName(customer.getFullName());
        double count = 0;
        
        for(FlowerOrder f : allOrders)
        {
            if((f.getCustomerId()).equals(allCustomers.get(0).getId()))
            {
                orders.add(f);
                count += f.getTotalCost().doubleValue();
            }
        }
        
        model.addAttribute("message","Total cost: $" + String.format("%.2f",count));
        
        //Get customer names
        HashMap<Integer, String> customers = orderBO.getCustomerNames();
        
        //Get Order Status
        HashMap<Integer, String> statuses = orderBO.getOrderStatuses();
        
        model.addAttribute("orders", orders);
        model.addAttribute("customers", customers);
        model.addAttribute("statuses", statuses);

        //Put the name in the model as well so it can be shown on the list view
        model.addAttribute("findNameMessage", " ("+customer.getFullName()+")");
        
        //send the user to the list page.
        return "orders/list";
    }
    
    /**
     * Page to allow user to export customers to a file
     *
     * @since 20201022
     * @author PAAG (modified from BJM (modified from Fred/Amro's project
     */
    @RequestMapping(value = "/export")
    public String export(Model model)
    {
        ArrayList<Customer> customers = loadCustomers();
        model.addAttribute("customers", customers);
        
        String PATH = "c:\\flowers\\";
        LocalDateTime now = LocalDateTime.now();
        String fileName = "customers_" + now.getYear() + now.getMonthValue() + now.getDayOfMonth() + now.getHour() + now.getMinute() + now.getSecond() + ".json";
        
        File myFile;
        try {
            Path path = Paths.get(PATH);
            try {
                Files.createDirectories(path);
            } catch (IOException ex) {
                System.out.println("Error creating directory");
            }

            myFile = new File(path + fileName);
            if (myFile.createNewFile()) {
                System.out.println("File is created!");
            } else {
                System.out.println("File already exists.");
            }

        } catch (IOException ex) {
            System.out.println("Error creating new file");
        }
        
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(PATH + fileName, false));
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Customer current : customers) {
            try {
                Gson gson = new Gson();
                String customerJson = gson.toJson(current);

                writer.write(customerJson + System.lineSeparator());
            } catch (IOException ex) {
                System.out.println("Error with file access");
            }

        }
        try {
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        model.addAttribute("findNameMessage","(Saved to file)");
        
        return "customers/list";
    }
    
    /**
     * Method to load customers with their type descriptions
     *
     * @since 20201022
     * @author PAAG (modified from BJM (modified from Fred/Amro's project
     */
    public ArrayList<Customer> loadCustomers() {
        ArrayList<Customer> customers = (ArrayList<Customer>) customerRepository.findAll();
        HashMap<Integer, String> customerTypesMap = CustomerTypeBO.getCustomerTypesMap();
        for (Customer current : customers) {
            current.setCustomerTypeDescription(customerTypesMap.get(current.getCustomerTypeId()));
        }
        return customers;

    }
    
}
