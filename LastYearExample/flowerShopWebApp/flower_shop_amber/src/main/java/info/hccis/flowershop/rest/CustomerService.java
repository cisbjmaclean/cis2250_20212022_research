package info.hccis.flowershop.rest;

import com.google.gson.Gson;
import info.hccis.flowershop.entity.jpa.Customer;
import info.hccis.flowershop.repositories.CustomerRepository;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Customer Service class for accessing customers using REST.
 *
 * @author PAAG
 * @since 20201116
 */
@Path("/CustomerService/customers")
public class CustomerService
{
    private final CustomerRepository cr;
    
    @Autowired
    public CustomerService(CustomerRepository cr){
        this.cr = cr;
    }
    
    /**
     * Method to get all customers using REST.
     * 
     * @author PAAG
     * @since 20201116
     * @return customers
     */
    @GET
    @Produces("application/json")
    public ArrayList<Customer> getAll()
    {
        ArrayList<Customer> customers = (ArrayList<Customer>) cr.findAll();
        return customers;
    }
    
    /**
     * Method to get a customer by their id using REST.
     * 
     * @author PAAG
     * @since 20201116
     * @return response
     */
    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getCustomerById(@PathParam("id") Integer id) throws URISyntaxException
    {
        Optional<Customer> customer = cr.findById(id);
         if (!customer.isPresent()) {
            return Response.status(204).build();
        } else {
            return Response
                    .status(200)
                    .entity(customer).build();
        }
    }
    
    /**
     * Method to create a customer using REST.
     * 
     * @author PAAG
     * @since 20201116
     * @return response
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(String customerJson)
    {
        try{
            String temp = save(customerJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }
    }
    
    /**
     * Method to update a customer using REST.
     * 
     * @author PAAG
     * @since 20201116
     * @return response
     */
    @PUT
    @Path("/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateCustomer(@javax.ws.rs.PathParam("id") int id, String customerJson) throws URISyntaxException 
    {

        try{
            String temp = save(customerJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }

    }
    
    /**
     * Method to make sure all required inputs are present.
     * 
     * @author PAAG
     * @since 20201116
     * @return string
     */
    public String save(String json) throws AllAttributesNeededException{
        
        Gson gson = new Gson();
        Customer customer = gson.fromJson(json, Customer.class);
        
        if(customer.getFullName() == null) {
            throw new AllAttributesNeededException("Please provide all mandatory inputs");
        }
 
        if(customer.getId() == null){
            customer.setId(0);
        }

        customer = cr.save(customer);

        String temp = "";
        temp = gson.toJson(customer);

        return temp;
        
        
    }
    
}
