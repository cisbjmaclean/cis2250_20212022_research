package info.hccis.flowershop.bo;

import info.hccis.flowershop.dao.CustomerDAO;
import info.hccis.flowershop.entity.jpa.Customer;

/**
 * Method to add a customer.
 * 
 * @author Parker
 * @since 20201022
 */
public class CustomerBO
{
    public static void addCustomer(Customer customer) {

        System.out.println("Customer object about to be added to the database"
                + ""
                + "\n" + customer.toString());

        //Add that customer to the database
        CustomerDAO customerDAO = new CustomerDAO();

        if (customer.getId() == 0) {
            customerDAO.insert(customer);
        } else {
            customerDAO.update(customer);
        }

    }
}
