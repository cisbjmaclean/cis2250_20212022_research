package info.hccis.cis2250.flowershop.ui.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import info.hccis.cis2250.flowershop.R;
import info.hccis.cis2250.flowershop.bo.Customer;
import info.hccis.cis2250.flowershop.ui.about.AboutViewModel;
import info.hccis.cis2250.flowershop.ui.customerList.CustomerListFragment;

public class ToolsFragment extends Fragment {

    private ToolsViewModel toolsViewModel;
    private ToggleButton toggleButton;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        toolsViewModel =
                new ViewModelProvider(this).get(ToolsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_tools, container, false);
        toolsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        toggleButton = root.findViewById(R.id.toggleButtonLoadFromRoom);

        boolean loadFromRoom;
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        loadFromRoom = sharedPref.getBoolean(getString(R.string.preference_load_from_room), false);
        toggleButton.setChecked(loadFromRoom);

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putBoolean(getString(R.string.preference_load_from_room), isChecked);
                editor.commit();
                Log.d("ma", "set to " + isChecked);
            }
        });


        return root;
    }

}