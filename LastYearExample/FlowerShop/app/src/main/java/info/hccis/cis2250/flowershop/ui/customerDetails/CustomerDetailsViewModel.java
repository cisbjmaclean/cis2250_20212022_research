package info.hccis.cis2250.flowershop.ui.customerDetails;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class CustomerDetailsViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public CustomerDetailsViewModel() {
        mText = new MutableLiveData<>();
        //mText.setValue("This is feedback fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}