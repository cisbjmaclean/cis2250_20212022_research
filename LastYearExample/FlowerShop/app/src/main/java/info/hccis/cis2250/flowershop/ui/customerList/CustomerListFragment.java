package info.hccis.cis2250.flowershop.ui.customerList;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.google.gson.internal.$Gson$Preconditions;

import info.hccis.cis2250.flowershop.R;
import info.hccis.cis2250.flowershop.bo.ApiWatcher;
import info.hccis.cis2250.flowershop.bo.Customer;
import info.hccis.cis2250.flowershop.bo.CustomerContent;
import info.hccis.cis2250.flowershop.util.NotificationApplication;
import info.hccis.cis2250.flowershop.util.NotificationUtil;


public class CustomerListFragment extends Fragment {

    private static ProgressBar progressBar;
    private static RecyclerView recyclerView;
    private static Context context;
    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private static ApiWatcher apiWatcher;

    public static RecyclerView getRecyclerView() {
        return recyclerView;
    }

    /**
     * Method to clear this fragments progress bar
     * @since 20200208
     * @author BJM
     */
    public static void clearProgressBarVisibility(){
        try {
            progressBar.setVisibility(View.GONE);
        }catch(Exception e){
            Log.d("ma","could not clear the progress bar - user may have left fragment.");
        }
    }
    public static void notifyDataChanged(String message){
        Log.d("ma", "Data changed:  "+message);
        NotificationApplication.setContext(context);
        NotificationUtil.sendNotification("Customers",message);

    }


    @Override
    public void onStart() {
        super.onStart();
        // Set the adapter
        context = getView().getContext();
        recyclerView = getView().findViewById(R.id.recyclerView);
        progressBar = getView().findViewById(R.id.progressBarLoadingCustomers);

        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }

        recyclerView.setAdapter(new RecyclerViewAdapter(CustomerContent.CUSTOMERS, mListener));


        CustomerContent.loadCustomers(getActivity());

        //**********************************************************************************************
        // Start a background thread which will watch the api and main thread know if there are changes.
        //**********************************************************************************************

        apiWatcher = new ApiWatcher();
        apiWatcher.setActivity(getActivity());
        apiWatcher.start();  //Start the background thread


    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_customer_list, container, false);
        // Inflate the layout for this fragment
        return view;
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(Customer item);
    }

}