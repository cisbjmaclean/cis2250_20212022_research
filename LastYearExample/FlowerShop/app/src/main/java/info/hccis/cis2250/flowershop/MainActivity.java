package info.hccis.cis2250.flowershop;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.MenuItem;
import android.view.Menu;
import android.view.View;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.room.Room;

import info.hccis.cis2250.flowershop.bo.Customer;
import info.hccis.cis2250.flowershop.dao.MyAppDatabase;
import info.hccis.cis2250.flowershop.ui.about.AboutFragment;
import info.hccis.cis2250.flowershop.ui.customerDetails.CustomerDetailsFragment;
import info.hccis.cis2250.flowershop.ui.customerList.CustomerListFragment;
import info.hccis.cis2250.flowershop.ui.tools.ToolsFragment;
import info.hccis.cis2250.flowershop.util.NotificationApplication;


public class MainActivity extends AppCompatActivity implements
        CustomerListFragment.OnListFragmentInteractionListener,
        CustomerDetailsFragment.OnFragmentInteractionListener{

    private AppBarConfiguration mAppBarConfiguration;
    public static MyAppDatabase myAppDatabase;
    private static NavController navController = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        NotificationApplication.setContext(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /*
         * Set onClickListener when click on fab to navigate to the customer fragment where to add a customer
         * Date: 15/02/2021
         * Author: Mariana Alkabalan
         * Purpose: Flower Shop Project
         */
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Add a Customer", Snackbar.LENGTH_LONG).show();
                getNavController().navigate(R.id.nav_customer);
            }
        });


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_about,R.id.nav_customer,
                R.id.nav_customer_list, R.id.nav_tools,R.id.nav_customer_details, R.id.nav_help,
                R.id.nav_map)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        myAppDatabase = Room.databaseBuilder(getApplicationContext(), MyAppDatabase.class, "customersdb").allowMainThreadQueries().build();

    }
    public static NavController getNavController() {
        return navController;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.onNavDestinationSelected(item, navController)
                || super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    /**
     * This method will be used in the customer fragment when the user clicks on a row of the
     * camper recyclerview.  This method will transfer the user to a details fragment.  The
     * id of the customer will be passed to the fragment and used to load the correct camper details.
     * Will use the arraylist associated with the recyclerview.
     *
     * @param item the customer
     * @author BJM
     * @since 20200124
     * Modified by Malkabalan - flowershop app - 2021-02-11
     */
    @Override
    public void onListFragmentInteraction(Customer item) {

        Log.d("ma", "item communicated from fragment: " + item.toString());

        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        bundle.putString("customer", gson.toJson(item));

        /*
          BJM 20200202
          Use the navigation controller object stored as an attribute of the main
          activity to navigate the ui to the customer detail fragment.
        */

        getNavController().navigate(R.id.nav_customer_details, bundle);

    }
    /**
     * This is the interaction from the details fragment.  I will want to add a new contact when
     * this button is pressed.
     *
     * @param item
     * @author BJM Modified by Mariana alkabalan 2021-03-14
     * @since 20200203
     */

    @Override
    public void onFragmentInteractionAddContact(Customer item) {

        /* Code to add a contact */
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.NAME, item.getFullName());
        startActivity(intent);
    }

    /**
     * This is the interaction from the details fragment.  I will want to share contact when
     * this button is pressed.
     *
     * @param item
     * @author BJM Modified by Mariana alkabalan 2021-03-14
     * @since 20200203
     */

    @Override
    public void onFragmentInteractionShareInfo(Customer item) {

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_SUBJECT, "flower shop notification");
        email.putExtra(Intent.EXTRA_TEXT, item.toString());

        //need this to prompts email client only
        email.setType("message/rfc822");
        startActivity(Intent.createChooser(email, "Choose an Email customer :"));


    }


}