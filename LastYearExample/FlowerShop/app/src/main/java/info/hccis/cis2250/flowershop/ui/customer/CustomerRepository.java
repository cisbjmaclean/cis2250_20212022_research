package info.hccis.cis2250.flowershop.ui.customer;
import info.hccis.cis2250.flowershop.util.JsonCustomerApi;
import info.hccis.cis2250.flowershop.util.Util;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/*
 * Author: Mariana Alkabalan
 * Date: 27.03.2021
 * Reference: https://learntodroid.com/how-to-send-json-data-in-a-post-request-in-android/
 */
public final class CustomerRepository {
    private static CustomerRepository instance;

    private JsonCustomerApi jsonCustomerApi;

    public static CustomerRepository getInstance() {
        if (instance == null) {
            instance = new CustomerRepository();
        }
        return instance;
    }

    public CustomerRepository() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Util.CUSTOMER_BASE_API)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        jsonCustomerApi = retrofit.create(JsonCustomerApi.class);
    }

    public JsonCustomerApi getCustomerService() {
        return jsonCustomerApi;
    }
}
