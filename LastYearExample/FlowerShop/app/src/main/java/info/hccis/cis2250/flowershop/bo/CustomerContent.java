package info.hccis.cis2250.flowershop.bo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import info.hccis.cis2250.flowershop.MainActivity;
import info.hccis.cis2250.flowershop.R;
import info.hccis.cis2250.flowershop.ui.customerList.CustomerListFragment;
import info.hccis.cis2250.flowershop.util.JsonCustomerApi;
import info.hccis.cis2250.flowershop.util.NotificationUtil;
import info.hccis.cis2250.flowershop.util.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class CustomerContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<Customer> CUSTOMERS = new ArrayList<Customer>();

    public static void reloadCustomersInRoom(List<Customer> customers)
    {
        MainActivity.myAppDatabase.customerDAO().deleteAll();
        for(Customer current : customers)
        {
            MainActivity.myAppDatabase.customerDAO().add(current);
        }
        Log.d("ma","loading customers from Room");
    }


    public static List<Customer> getCustomersFromRoom()
    {
        Log.d("ma","Loading customers from Room");

        List<Customer> customersBack = MainActivity.myAppDatabase.customerDAO().get();
        Log.d("ma","Number of customers loaded: " + customersBack.size());
        for(Customer current : customersBack)
        {
            Log.d("ma",current.toString());
        }
        return customersBack;
    }

    /**
     * Load the customers.  This method will use the rest service to provide the data.
     * This is the list which is used to back the RecyclerView.
     *
     */

    public static void loadCustomers(Activity context) {

        Log.d("ma", "Accessing api at:" + Util.CUSTOMER_BASE_API);

        //Use Retrofit to connect to the service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Util.CUSTOMER_BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonCustomerApi jsonCustomerApi = retrofit.create(JsonCustomerApi.class);

        //Create a list of customers.
        Call<List<Customer>> call = jsonCustomerApi.getCustomers();

        //final reference to activity to get shared preferences
        final Activity contextIn = context;

        call.enqueue(new Callback<List<Customer>>() {

            @Override
            public void onResponse(Call<List<Customer>> call, Response<List<Customer>> response) {

                List<Customer> customers;

                if(!response.isSuccessful())
                {
                    Log.d("MA-Rest","Unsuccessful response from rest: " + response.code());
                    SharedPreferences sharedPref = contextIn.getPreferences(Context.MODE_PRIVATE);
                    boolean preferToLoad = sharedPref.getBoolean(contextIn.getString(R.string.preference_load_from_room),false);
                    if(preferToLoad)
                    {
                        customers = getCustomersFromRoom();
                        NotificationUtil.sendNotification("Customers loaded", "(" + customers.size() + ") Customers loaded from Room");
                    } else {
                        NotificationUtil.sendNotification("Customers not loaded", "Cannot load Customers - Connection Error");
                        return;
                    }
                } else {
                    customers = response.body();
                    NotificationUtil.sendNotification("Customers loaded", "(" + customers.size() + ") Customers loaded from API service");

                    //Have successfully connected.  Reload the camper database.
                    reloadCustomersInRoom(customers);

                }

                Log.d("ma", "data back from service call #returned=" + customers.size());

                //**********************************************************************************
                // Now that we have the customers, will use them to assign values to the list which
                // is backing the recycler view.
                //**********************************************************************************

                CustomerContent.CUSTOMERS.clear();
                CustomerContent.CUSTOMERS.addAll(customers);

                //**********************************************************************************
                // The CustomerListFragment has a recyclerview which is used to show the customer list.
                // This recyclerview is backed by the customer list in the CustomerContent class.  After
                // this list is loaded, need to notify the adapter from the recyclerview that the
                // data is changed.
                //**********************************************************************************

                CustomerListFragment.getRecyclerView().getAdapter().notifyDataSetChanged();
                //Remove the progress bar from the view.
                CustomerListFragment.clearProgressBarVisibility();

            }

            @Override
            public void onFailure(Call<List<Customer>> call, Throwable t) {
                // If the api call failed, give a notification to the user.
                Log.d("bjm", "api call failed");
                Log.d("bjm", t.getMessage());

                SharedPreferences sharedPref = contextIn.getPreferences(Context.MODE_PRIVATE);

                boolean preferToLoad = sharedPref.getBoolean(contextIn.getString(R.string.preference_load_from_room),false);

                CustomerContent.CUSTOMERS.clear();
                if (preferToLoad) {
                    CustomerContent.CUSTOMERS.addAll(getCustomersFromRoom());
                    //Remove the progress bar from the view.
                    NotificationUtil.sendNotification("Customers loaded", "(" + CustomerContent.CUSTOMERS.size() + ") Customers loaded from room");
                } else {
                    CustomerContent.CUSTOMERS.clear();
                    //Remove the progress bar from the view.
                    NotificationUtil.sendNotification("Customers not loaded", "Cannot load Customers - Connection Error");
                }
                CustomerListFragment.clearProgressBarVisibility();
                CustomerListFragment.getRecyclerView().getAdapter().notifyDataSetChanged();
            }
        });
    }
}