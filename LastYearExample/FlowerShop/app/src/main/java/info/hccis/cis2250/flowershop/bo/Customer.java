package info.hccis.cis2250.flowershop.bo;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

import info.hccis.cis2250.flowershop.util.Util;
import com.google.gson.annotations.SerializedName;

@Entity(tableName = "customer")
public class Customer implements Serializable
{
    public static final String CUSTOMER_BASE_API = Util.BASE_SERVER+"/api/CustomerService/";
    private static final long serialVersionUID = 1L;
    @ColumnInfo(name = "id")
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    @SerializedName("fullName")
    @ColumnInfo(name = "fullName")
    private String fullName;

    @SerializedName("address1")
    @ColumnInfo(name = "address1")
    private String address1;

    @SerializedName("city")
    @ColumnInfo(name = "city")
    private String city;

    @SerializedName("province")
    @ColumnInfo(name = "province")
    private String province;

    @SerializedName("postalCode")
    @ColumnInfo(name = "postalCode")
    private String postalCode;

    @SerializedName("phoneNumber")
    @ColumnInfo(name = "phoneNumber")
    private String phoneNumber;

    @SerializedName("birthDate")
    @ColumnInfo(name = "birthDate")
    private String birthDate;

    @SerializedName("loyaltyCard")
    @ColumnInfo(name = "loyaltyCard")
    private String loyaltyCard;

    @SerializedName("customerTypeDescription")
    @ColumnInfo(name = "customerTypeDescription")
    private String customerTypeDescription;

    public String getCustomerTypeDescription() {
        return customerTypeDescription;
    }

    public void setCustomerTypeDescription(String customerTypeDescription) {
        this.customerTypeDescription = customerTypeDescription;
    }

    public Customer()
    {
    }

    public Customer(String fullName) {
        this.fullName = fullName;
    }

    public Customer(String fullName, String address1, String city, String province, String postalCode, String phoneNumber, String birthDate, String loyaltyCard) {
        this.fullName = fullName;
        this.address1 = address1;
        this.city = city;
        this.province = province;
        this.postalCode = postalCode;
        this.phoneNumber = phoneNumber;
        this.birthDate = birthDate;
        this.loyaltyCard = loyaltyCard;
    }

    public Customer(Integer id)
    {
        this.id = id;
    }

    public Customer(Integer id, String fullName)
    {
        this.id = id;
        this.fullName = fullName;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getAddress1()
    {
        return address1;
    }

    public void setAddress1(String address1)
    {
        this.address1 = address1;
    }

    public String getCity()
    {
        return city;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public String getProvince()
    {
        return province;
    }

    public void setProvince(String province)
    {
        this.province = province;
    }

    public String getPostalCode()
    {
        return postalCode;
    }

    public void setPostalCode(String postalCode)
    {
        this.postalCode = postalCode;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthDate()
    {
        return birthDate;
    }

    public void setBirthDate(String birthDate)
    {
        this.birthDate = birthDate;
    }

    public String getLoyaltyCard()
    {
        return loyaltyCard;
    }

    public void setLoyaltyCard(String loyaltyCard)
    {
        this.loyaltyCard = loyaltyCard;
    }

    public String getCustomerInitial(){
        String initial = "";
        if(fullName.contains(" ")){
            int firstSpace = fullName.indexOf(" ");
            String firstName = fullName.substring(0, firstSpace);
            String lastName = fullName.substring(firstSpace + 1);
            initial = firstName.charAt(0) + "" + lastName.charAt(0);
        }else{
            initial = fullName.charAt(0)+"";
        }
        return initial.toUpperCase();
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customer)) {
            return false;
        }
        Customer other = (Customer) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "Name: "+fullName+ " birth date: "+birthDate;
    }

}

