package info.hccis.cis2250.flowershop.util;

import java.util.List;
import java.util.Map;

import info.hccis.cis2250.flowershop.bo.Customer;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface JsonCustomerApi {

    /**
     * This abstract method to be created to allow retrofit to get list of customers
     * @return List of customers
     * @since 20200202
     * @author BJM (with help from the retrofit research.
     */

    @GET("customers")
    Call<List<Customer>> getCustomers();
    @POST("customers")
    Call<Customer> createCustomer(@Body Customer customer);

    /* not used
    @FormUrlEncoded
    @POST("customers")
    Call<Customer> createCustomer(@Field("fullName") String fullName, @Field("address1") String address1, @Field("city") String city,
    @Field("province") String province, @Field("postalCode") String postalCode, @Field("phoneNumber") String phoneNumber, @Field("birthDate") String birthDate, @Field("loyaltyCard") String loyaltyCard);
    */

}
