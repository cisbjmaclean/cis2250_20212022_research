package info.hccis.cis2250.flowershop.ui.customerDetails;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.Gson;

import info.hccis.cis2250.flowershop.R;
import info.hccis.cis2250.flowershop.bo.Customer;
import info.hccis.cis2250.flowershop.ui.customerList.CustomerListFragment;

public class CustomerDetailsFragment extends Fragment {

    private CustomerDetailsViewModel customerDetailsViewModel;
    private Customer customer;
    private TextView tvName;
    private TextView tvAddress;
    private TextView tvCity;
    private TextView tvProvince;
    private TextView tvPostalCode;
    private TextView tvPhoneNumber;
    private TextView tvDOB;
    private Button btnAddContact;
    private Button btnShareInfo;
    private OnFragmentInteractionListener mListener;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*
          BJM 20200202
          modified by malkabalan
          Note how the arguments are passed from the activity to this fragment.  The
          customer object that was chosen on the recyclerview was encoded as a json string and then
          decoded when this fragment is accessed.
         */

        if (getArguments() != null) {
            String customerJson = getArguments().getString("customer");
            Gson gson = new Gson();
            customer = gson.fromJson(customerJson, Customer.class);
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        customerDetailsViewModel =
                new ViewModelProvider(this).get(CustomerDetailsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_customer_details, container, false);
        customerDetailsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }
        });

        tvName = root.findViewById(R.id.textViewCustomerName);
        tvDOB = root.findViewById(R.id.textViewDob);
        tvAddress = root.findViewById(R.id.textViewAddress);
        tvCity = root.findViewById(R.id.textViewCustomerCity);
        tvPhoneNumber = root.findViewById(R.id.textViewPhoneNumber);
        tvProvince = root.findViewById(R.id.textViewCustomerProvince);
        tvPostalCode = root.findViewById(R.id.textViewPostalCode);
        tvPhoneNumber = root.findViewById(R.id.textViewPhoneNumber);

        tvName.setText(customer.getFullName());
        tvDOB.setText(customer.getBirthDate());
        tvAddress.setText(customer.getAddress1());
        tvCity.setText(customer.getCity());
        tvProvince.setText(customer.getProvince());
        tvPostalCode.setText(customer.getPostalCode());
        tvPhoneNumber.setText(customer.getPhoneNumber());






        /*
          taken from research of last year 2019/2020
          When the add button is clicked, will pass the camper object to the activity implementation
          method.  The MainActivity will then take care of adding the contact.
         */
        btnAddContact = root.findViewById(R.id.btnAddContact);
        btnAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionAddContact(customer);
            }
        });

        /*
          taken from research of last year 2019/2020
          Similar to the add contact button, to send an email will notify the MainActivity and
          let it take care of sending the email with the information passed in the camper.
         */

        btnShareInfo = root.findViewById(R.id.btnShareInfo);
        btnShareInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteractionShareInfo(customer);
            }
        });




        return root;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof CustomerListFragment.OnListFragmentInteractionListener) {
            mListener = (CustomerDetailsFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteractionAddContact(Customer item);
        void onFragmentInteractionShareInfo(Customer customer);
    }
}