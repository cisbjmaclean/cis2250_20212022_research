package info.hccis.cis2250.flowershop;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

public class IntroActivity extends AppCompatActivity {
    //timer in miliseconds, 1000ms = 1s//
    private static int SPLASH_TIME_OUT = 4000;
    ImageView imageViewLogo;
    ImageView imageViewBg;
    TextView textViewTitle;
    LottieAnimationView lottieAnimationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        new Handler().postDelayed(new Runnable() {
            //showing splashscreen with a timer //

            @Override
            public void run() {
                //this is executed once the timer is over//

                Intent i = new Intent(IntroActivity.this,    MainActivity.class);
                startActivity(i);
                finish();

            }
        },SPLASH_TIME_OUT);
        imageViewLogo = findViewById(R.id.splashLogo);
        textViewTitle = findViewById(R.id.textViewTitle);
        imageViewBg = findViewById(R.id.splashBg);
        lottieAnimationView = findViewById(R.id.lottieAnimation);

        imageViewBg.animate().translationY(-1600).setDuration(1000).setStartDelay(4000);
        imageViewLogo.animate().translationY(-1400).setDuration(1000).setStartDelay(4000);
        textViewTitle.animate().translationY(-1400).setDuration(1000).setStartDelay(4000);
        lottieAnimationView.animate().translationY(-1400).setDuration(1000).setStartDelay(4000);

    }
}