package info.hccis.cis2250.flowershop.ui.customer;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import info.hccis.cis2250.flowershop.R;
import info.hccis.cis2250.flowershop.bo.Customer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CustomerFragment extends Fragment {

    private CustomerViewModel customerViewModel;
    private EditText editTextCustomerName;
    private EditText editTextAddress;
    private EditText editTextPhoneNumber;
    private EditText editTextCity;
    private EditText editTextProvince;
    private EditText editTextPostalCode;
    private EditText editTextDateOfBirth;
    private EditText editTextLoyaltyCard;
    private Button buttonSubmit;

    private CustomerRepository customerRepository;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        customerViewModel =
                new ViewModelProvider(this).get(CustomerViewModel.class);
        View root = inflater.inflate(R.layout.fragment_customer, container, false);
        //final TextView textView = root.findViewById(R.id.text_home);
        customerViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                //textView.setText(s);
            }

        });
        editTextCustomerName = root.findViewById(R.id.editTextTextCustomerName);
        editTextPhoneNumber = root.findViewById(R.id.editTextPhone);
        editTextAddress = root.findViewById(R.id.editTextTextCustomerAddress);
        editTextDateOfBirth = root.findViewById(R.id.editTextDate);
        editTextProvince = root.findViewById(R.id.editTextTextCustomerProvince);
        editTextCity = root.findViewById(R.id.editTextTextCustomerCity);
        editTextPostalCode = root.findViewById(R.id.editTextTextPostalCode);
        editTextLoyaltyCard = root.findViewById(R.id.editTextTextLoyaltyCard);
        customerRepository = customerRepository.getInstance();



        //submit button
        buttonSubmit = root.findViewById(R.id.btnSubmit);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Customer c = new Customer(
                        editTextCustomerName.getText().toString(),
                        editTextAddress.getText().toString(),
                        editTextCity.getText().toString(),
                        editTextProvince.getText().toString(),
                        editTextPostalCode.getText().toString(),
                        editTextPhoneNumber.getText().toString(),
                        editTextDateOfBirth.getText().toString(),
                        editTextLoyaltyCard.getText().toString()
                );

                //customerRepository.getCustomerService().createCustomer(c.getFullName(), c.getAddress1(), c.getCity(),c.getProvince(), c.getPostalCode(), c.getPhoneNumber(), c.getBirthDate(), c.getLoyaltyCard()).enqueue(new Callback<Customer>() {
                customerRepository.getCustomerService().createCustomer(c).enqueue(new Callback<Customer>() {
                    @Override
                    public void onResponse(Call<Customer> call, Response<Customer> r) {
                        Toast.makeText(getContext(), "Customer " + r.body().getFullName() + " is successfully added!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<Customer> call, Throwable t) {
                        Toast.makeText(getContext(), "Error Adding Customer: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                return;
            }

        });

       return root;
    }
}