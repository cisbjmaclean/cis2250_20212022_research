package info.hccis.cis2250.flowershop.ui.customerList;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import info.hccis.cis2250.flowershop.R;

import info.hccis.cis2250.flowershop.bo.Customer;

import java.util.ArrayList;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{

    private final List<Customer> mValues;
    private final CustomerListFragment.OnListFragmentInteractionListener mListener;
    //private List<Customer> filteredCustomerList;

    public RecyclerViewAdapter(List<Customer> items, CustomerListFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
        //filteredCustomerList = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_customer_row, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mInitialView.setText(mValues.get(position).getCustomerInitial());
        holder.mfullNameView.setText(mValues.get(position).getFullName());
        holder.mBirthDateView.setText(mValues.get(position).getBirthDate());
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    /**
     * <p>Returns a filter that can be used to constrain data with a filtering
     * pattern.</p>
     *
     * @return a filter used to constrain data
     * unused code -  tried to implement search view with recycler view
     */
    /*
    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charSequenceString = charSequence.toString();
                if (charSequenceString.isEmpty()) {
                    filteredCustomerList = mValues;
                } else {
                    List<Customer> filteredList = new ArrayList<>();
                    for (Customer customer : mValues) {
                        if (customer.getFullName().toLowerCase().contains(charSequenceString.toLowerCase())) {
                            filteredList.add(customer);
                        }

                    }
                    filteredCustomerList = filteredList;
                }
                FilterResults results = new FilterResults();
                results.values = filteredCustomerList;
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                filteredCustomerList = (List<Customer>) results.values;
                notifyDataSetChanged();
            }
        };
    }*/

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mInitialView;
        public final TextView mfullNameView;
        public final TextView mBirthDateView;

        public Customer mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mInitialView = (TextView) view.findViewById(R.id.circleInitial);
            mfullNameView = (TextView) view.findViewById(R.id.textViewCustomerFullName);
            mBirthDateView = (TextView) view.findViewById(R.id.textViewCusBirthDate);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mfullNameView.toString() + "'" + mBirthDateView.toString();
        }
    }
}
