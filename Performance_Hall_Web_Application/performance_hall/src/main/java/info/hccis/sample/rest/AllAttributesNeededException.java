/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.sample.rest;

/**
 * Used if all attributes not provided
 * @author bjm
 * @since 6-Nov-2020
 */
public class AllAttributesNeededException extends Exception{
    public AllAttributesNeededException(String message){
        super(message);
    }
}
